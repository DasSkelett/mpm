package main

import (
	"context"
	"crypto/rand"
	"errors"
	"flag"
	"fmt"
	"math/big"

	"github.com/jackc/pgx/v5"
	"golang.org/x/crypto/bcrypt"
)

type AddCommand struct {
	fs *flag.FlagSet

	username string
	domain   string
	appName  string

	db *pgx.Conn
}

func NewAddCommand() *AddCommand {
	cmd := &AddCommand{
		fs: flag.NewFlagSet("add", flag.ContinueOnError),
	}

	cmd.fs.StringVar(&cmd.username, "user", "", "username (first) part of the e-mail address of the account to generate a new app password for")
	cmd.fs.StringVar(&cmd.domain, "domain", "", "domain (second) part of the e-mail address of the account to generate a new app password for")
	cmd.fs.StringVar(&cmd.appName, "app-name", "", "name of the app to generate a password for")

	return cmd
}

func (c *AddCommand) Name() string {
	return c.fs.Name()
}

func (c *AddCommand) Init(args []string) error {
	err := c.fs.Parse(args)
	if err != nil {
		return err
	}

	if c.username == "" || c.domain == "" || c.appName == "" {
		return errors.New("--username, --domain and --app-name are required")
	}

	c.db, err = initiateDBConn()
	return err
}

func (c *AddCommand) Run() error {
	ctx := context.Background()
	defer c.db.Close(ctx)
	fmt.Println("Generating app password for", c.username+"@"+c.domain+":\n")

	var userID string
	err := c.db.QueryRow(ctx,
		"SELECT id FROM users WHERE users.username = $1 AND users.domain = $2",
		c.username, c.domain).Scan(&userID)
	if err != nil {
		return err
	}

	password, err := GenerateRandomString(32)
	if err != nil {
		return err
	}
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		return err
	}

	appName := c.username + ":" + c.appName

	var appID int
	err = c.db.QueryRow(ctx,
		"INSERT INTO app_passwords (user_id, app, password) VALUES ($1, $2, $3) RETURNING id, app",
		userID, appName, passwordHash).Scan(&appID, &appName)
	if err != nil {
		return err
	}

	fmt.Println("App Id:", appID)
	fmt.Println("App name:", appName)
	fmt.Println(">Password:", password)

	fmt.Println("\nNew app password generated. Use the app name prefixed with your username and the colon as login username.")

	return nil
}

var letterPool = []rune("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-+_*,.;:#'?!()[]{}/&%$€<>")

func GenerateRandomString(length int) (string, error) {
	ret := make([]rune, length)
	for i := 0; i < length; i++ {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(letterPool))))
		if err != nil {
			return "", err
		}
		ret[i] = letterPool[num.Int64()]
	}

	return string(ret), nil
}
