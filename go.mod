module gitlab.com/DasSkelett/mpm

go 1.19

require (
	github.com/jackc/pgx/v5 v5.0.4
	golang.org/x/crypto v0.1.0
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	golang.org/x/text v0.4.0 // indirect
)
