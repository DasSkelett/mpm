# mpm

mpm, short for mail password management.

A simple tool to manage application passwords stored in PostgreSQL for my self-hosted Postfix+Dovecot setup.
