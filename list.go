package main

import (
	"context"
	"flag"
	"fmt"

	"github.com/jackc/pgx/v5"
)

type ListCommand struct {
	fs *flag.FlagSet

	username string
	domain   string

	db *pgx.Conn
}

func NewListCommand() *ListCommand {
	cmd := &ListCommand{
		fs: flag.NewFlagSet("list", flag.ContinueOnError),
	}

	cmd.fs.StringVar(&cmd.username, "user", "", "username (first) part of the e-mail address of the account to list app passwords for")
	cmd.fs.StringVar(&cmd.domain, "domain", "", "domain (second) part of the e-mail address of the account to list app passwords for")

	return cmd
}

func (c *ListCommand) Name() string {
	return c.fs.Name()
}

func (c *ListCommand) Init(args []string) error {
	err := c.fs.Parse(args)
	if err != nil {
		return err
	}
	c.db, err = initiateDBConn()
	return err
}

func (c *ListCommand) Run() error {
	ctx := context.Background()
	defer c.db.Close(ctx)
	fmt.Println("Listing app passwords for", c.username+"@"+c.domain+":\n")

	rows, err := c.db.Query(ctx,
		"SELECT app_passwords.id, app_passwords.app FROM app_passwords INNER JOIN users ON app_passwords.user_id = users.id WHERE users.username = $1 AND users.domain = $2",
		c.username, c.domain)
	if err != nil {
		return err
	}

	fmt.Println("| App Id | App Name |")

	var id int
	var app string
	_, err = pgx.ForEachRow(rows, []any{&id, &app}, func() error {
		fmt.Println("|", id, "|", app, "|")
		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
