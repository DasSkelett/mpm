package main

import (
	"bufio"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/jackc/pgx/v5"
)

type Command interface {
	Init([]string) error
	Run() error
	Name() string
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("You must pass a sub-command")
		os.Exit(1)
	}

	cmds := []Command{
		NewAddCommand(),
		NewListCommand(),
	}

	subcommand := os.Args[1]

	if subcommand == "help" || subcommand == "--help" || subcommand == "-help" {
		fmt.Println("Available commands:")
		for _, cmd := range cmds {
			fmt.Println("*", cmd.Name())
		}
		os.Exit(0)
	}

	for _, cmd := range cmds {
		if cmd.Name() == subcommand {
			err := cmd.Init(os.Args[2:])
			if err != nil {
				if errors.Is(err, flag.ErrHelp) {
					// No error condition
					os.Exit(0)
				}

				err = fmt.Errorf("command initialization failed: %w", err)
				fmt.Println(err)
				os.Exit(1)
			}
			err = cmd.Run()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			return
		}
	}

	fmt.Printf("Unknown subcommand: %s\n", subcommand)
	os.Exit(1)
}

func initiateDBConn() (*pgx.Conn, error) {
	dbConnString := os.Getenv("MPM_DB_CONNECTION_STRING")
	if dbConnString == "" {
		fmt.Println("Please enter the database connection string in the form of postgres://<username>:<password>@<host>:<port>/<database>")
		stdinReader := bufio.NewReader(os.Stdin)
		input, _ := stdinReader.ReadString('\n')
		fmt.Println()
		dbConnString = strings.TrimSpace(input)
	}
	return pgx.Connect(context.Background(), dbConnString)
}
